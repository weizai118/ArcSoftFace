﻿using System;

namespace ArcSoftFaceAforge
{
    /**
     * 人脸识别结果
     * */
    internal class FaceIdentifierResult
    {
        public FaceIdentifierResult(string identifier, string name, DateTime time, string imageFile)
        {
            Identifier = identifier;
            Name = name;
            Time = time;
            ImageFile = imageFile;
        }

        public string Identifier { get; set; }

        public string Name { get; set; }

        public DateTime Time { get; set; }

        public string ImageFile { get; set; }
    }
}